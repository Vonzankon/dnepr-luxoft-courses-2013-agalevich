package com.luxoft.dnepr.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DummyServlet extends HttpServlet {
    private static Map<String, String> dataBase = new ConcurrentHashMap();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override

    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder content = new StringBuilder("");

        String name = request.getParameter("name");
        String age = request.getParameter("age");

        if (name == null || name.equals("") ||
                age == null || age.equals("")) {

            content.append("{\"error\": \"Illegal parameters\"}");


            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("application/json; charset=utf-8");
            response.setContentLength(content.toString().getBytes().length);
            PrintWriter writer = response.getWriter();
            writer.println(content.toString());
            writer.close(); //???
        }

        if (dataBase.containsKey(name)) {
            content.setLength(0);
            content.append(String.format("{\"error\": \"Name ${%1$s} already exists\"}", name));

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("application/json; charset=utf-8");
            response.setContentLength(content.toString().getBytes().length);
            PrintWriter writer = response.getWriter();
            writer.println(content.toString());
            writer.close(); //???

        } else {
            dataBase.put(name, age);
            response.setStatus(HttpServletResponse.SC_CREATED);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");

        String name = request.getParameter("name");

        String age = request.getParameter("age");

        if (name == null || name.equals("") ||
                age == null || age.equals("")) {
            content.append("{\"error\": \"Illegal parameters\"}");

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("application/json; charset=utf-8");
            response.setContentLength(content.toString().getBytes().length);
            PrintWriter writer = response.getWriter();
            writer.println(content.toString());
            writer.close();  //???
        }

        if (!dataBase.containsKey(name)) {
            content.setLength(0);
            content.append(String.format("{\"error\": \"Name ${%1$s} does not exist\"}", name));
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("application/json; charset=utf-8");
            response.setContentLength(content.toString().getBytes().length);
            PrintWriter writer = response.getWriter();
            writer.println(content.toString());
            writer.close(); //???
        } else {
            dataBase.put(name, age);
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        }


    }


}
