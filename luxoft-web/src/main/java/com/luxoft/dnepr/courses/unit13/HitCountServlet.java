package com.luxoft.dnepr.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class HitCountServlet extends HttpServlet {
    private static AtomicLong enterCounter = new AtomicLong(0);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        enterCounter.incrementAndGet();
        StringBuilder content = new StringBuilder("{\"hitCount\": ");
        content.append(enterCounter).append("}");

        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json; charset=utf-8");
        response.setContentLength(content.toString().getBytes().length);


        PrintWriter writer = response.getWriter();
        writer.println(content.toString());
        writer.close(); //???
    }
}
