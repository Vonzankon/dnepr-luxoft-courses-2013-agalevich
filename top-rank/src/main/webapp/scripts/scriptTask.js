$(document).ready(function () {
    $("#searchInput").focus(function () {
        if ($("#searchInput").val() === "Please,enter the search string") {
            $("#searchInput").val("");
        }
    });
});
$("#searchInput").blur(function () {
    if ($("#searchInput").val() === "") {
        $("#searchInput").val("Please,enter the search string");
    }
});

var dummyData = [
    { href : 'jquery.com', description : 'JQuery: very popular lightweight javascript library' },
    { href : 'w3.org', description : 'World wide web consorcium main web site' },
    { href : 'mozilla.org', description : 'Mozilla foundation resources' },
    { href : 'ru.wikipedia.org', description : 'Russian division of wikipedia - free internet enciclopedia' }
];

$("#buttonClick").click(function(){
    $(".container").empty();

    for (var i = 0; i < dummyData.length;i++)
    {
        var temp = "<div class=\"content\"><a href=\"http://"+dummyData[i].href+
            "\">"+dummyData[i].href+"</a>"+"<p>"+dummyData[i].description+"</p></div>";
        $(".container").append(temp);
    }


});
