package com.luxoft.dnepr.courses.regular.unit2;

public abstract class AbstractProduct implements Product, Cloneable {

    private String code;
    private String name;
    private double price;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProduct that = (AbstractProduct) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        return 31 * result + (name != null ? name.hashCode() : 0);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {

        return (AbstractProduct) super.clone();
    }

}
