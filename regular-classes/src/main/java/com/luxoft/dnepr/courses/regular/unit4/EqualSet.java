package com.luxoft.dnepr.courses.regular.unit4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class EqualSet<E> implements Set<E> {

  private ArrayList<E> equalSet = null;

    public EqualSet() {
        equalSet = new ArrayList<E>();
    }

    public EqualSet (Collection<? extends E> collection){
        addAll(collection);
    }
    @Override
    public int size() {
       return this.equalSet.size();
    }


    @Override
    public boolean isEmpty() {
        return this.equalSet.isEmpty();
    }


    @Override
    public boolean contains(Object o) {
        return this.equalSet.contains(o);
    }


    @Override
    public Iterator<E> iterator() {
        return this.equalSet.iterator();
    }


    @Override
    public Object[] toArray() {
        return this.equalSet.toArray();
    }


    @Override
    public <T> T[] toArray(T[] a) {
        return this.equalSet.toArray(a);
    }


    @Override
    public boolean add(E e) {
        if (contains(e)) return false;
        return this.equalSet.add(e);
    }


    @Override
    public boolean remove(Object o) {
      return this.equalSet.remove(o);
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        return this.equalSet.containsAll(c);
    }


    @Override
    public boolean addAll(Collection<? extends E> c) {
       boolean result = false;
        for (E e: c){
            result = (add(e)||result);
        }
        return result;
    }


    @Override
    public boolean retainAll(Collection<?> c) {
        return this.equalSet.retainAll(c);
    }


    @Override
    public boolean removeAll(Collection<?> c) {
        return this.equalSet.removeAll(c);
    }

    @Override
    public void clear() {
      equalSet.clear();
    }
}
