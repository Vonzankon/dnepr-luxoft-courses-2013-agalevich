package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private List<CompositeProduct> compositeProductList = new ArrayList<CompositeProduct>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {

        for (CompositeProduct tempProduct : compositeProductList) {
            if (tempProduct.getFirstProduct().equals(product)) {
                tempProduct.add(product);
                return;
            }
        }
        CompositeProduct tempList = new CompositeProduct();
        tempList.add(product);
        compositeProductList.add(tempList);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        if (compositeProductList.isEmpty()) return 0;

        double sum = 0;
        for (CompositeProduct compositeProduct : compositeProductList) {
            sum += compositeProduct.getPrice();
        }
        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {


        List<Product> sortedProductsList = new ArrayList<Product>();
        for (Product product : compositeProductList) {

            sortedProductsList.add(product);

        }
        Collections.sort(sortedProductsList, new SortedListByPrice());
        return sortedProductsList;


    }


    @Override
    public String toString() {
        List<String> productListView = new ArrayList<String>();
        for (Product product : getProducts()) {
            productListView.add(product.toString());
        }
        return productListView.toString() + "\nTotal cost: " + summarize();
    }

    class SortedListByPrice implements Comparator<Product> {


        public int compare(Product o1, Product o2) {
            double price1 = ((Product) o1).getPrice();
            double price2 = ((Product) o2).getPrice();

            if (price1 > price2)
                return -1;
            else if (price1 < price2)
                return 1;
            else
                return 0;
        }
    }

}
