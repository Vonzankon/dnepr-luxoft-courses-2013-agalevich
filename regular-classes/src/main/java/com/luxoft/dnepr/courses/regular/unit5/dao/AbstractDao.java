package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {


    @Override
    public E save(E entity) throws UserAlreadyExist {
        if (entity.getId() == null) {
            entity.setId(getMaxId() + 1);
        } else {
            if (containsKey(entity.getId())) {
                throw new UserAlreadyExist("User with the same id(" + entity.getId() + ") is already exist");
            }
        }
        EntityStorage.getEntities().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public E update(Entity entity) throws UserNotFound {
        if (entity.getId() == null) {
            throw new UserNotFound("User with ID " + entity.getId() + "not found.");
        }
        return (E) EntityStorage.getEntities().put(entity.getId(), entity);
    }

    @Override
    public E get(long id) {
        if (!EntityStorage.getEntities().containsKey(id)) {
            return null;
        }
        return (E) EntityStorage.getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        if (!EntityStorage.getEntities().containsKey(id)) {
            return false;
        }
        EntityStorage.getEntities().remove(id);
        return true;
    }

    private boolean containsKey(Long key) {
        for (Long id : EntityStorage.getEntities().keySet()) {
            if (id.equals(key))
                return true;
        }
        return false;
    }

    private Long getMaxId() {
        long maxId = 0;
        for (Long id : EntityStorage.getEntities().keySet()) {
            if (maxId < id)
                maxId = id;
        }
        return maxId;
    }
}
