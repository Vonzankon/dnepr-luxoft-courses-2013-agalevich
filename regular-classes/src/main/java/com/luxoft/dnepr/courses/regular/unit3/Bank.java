package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collections;
import java.util.Map;

public class Bank implements BankInterface {

    private Map<Long, UserInterface> users;

    public Bank(String expectedJavaVersion) {
        String javaVersion = System.getProperty("java.version");
        if (!javaVersion.startsWith(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(javaVersion, expectedJavaVersion, "");
        }
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return Collections.unmodifiableMap(users);
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount)
            throws NoUserFoundException, TransactionException {
        UserInterface fromUser = users.get(fromUserId);
        UserInterface toUser = users.get(toUserId);
        if (fromUser == null) {
            throw new NoUserFoundException(fromUserId, "");
        }
        if (toUser == null) {
            throw new NoUserFoundException(toUserId, "");
        }
        try {
            checkTransaction(fromUser, toUser, amount);
        } catch (WalletIsBlockedException e) {
            UserInterface user = getUserByWallet(e.getWalletId());
            throw new TransactionException(String.format("User '%s' wallet is blocked", user.getName()));
        } catch (InsufficientWalletAmountException e) {
            UserInterface user = getUserByWallet(e.getWalletId());
            throw new TransactionException(String.format("User '%s' has insufficient funds (%s < %s)"
                    , user.getName()
                    , format(user.getWallet().getAmount())
                    , format(amount)));
        } catch (LimitExceededException e) {
            UserInterface user = getUserByWallet(e.getWalletId());
            throw new TransactionException(String.format("User '%s' wallet limit exceeded (%s + %s > %s)"
                    , user.getName()
                    , format(user.getWallet().getAmount())
                    , format(amount)
                    , format(user.getWallet().getMaxAmount())));
        }
        processTransaction(fromUser, toUser, amount);
    }

    private void checkTransaction(UserInterface fromUser, UserInterface toUser, BigDecimal amount)
            throws WalletIsBlockedException, InsufficientWalletAmountException, LimitExceededException {
        if (fromUser.getWallet().getStatus() == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(fromUser.getWallet().getId(), "");
        }
        if (toUser.getWallet().getStatus() == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(toUser.getWallet().getId(), "");
        }
        fromUser.getWallet().checkWithdrawal(amount);
        toUser.getWallet().checkTransfer(amount);
    }

    private void processTransaction(UserInterface fromUser, UserInterface toUser, BigDecimal amount) {
        fromUser.getWallet().withdraw(amount);
        toUser.getWallet().transfer(amount);
    }

    private UserInterface getUserByWallet(Long walletId) {
        for (UserInterface user : users.values()) {
            if (user.getWallet().getId().equals(walletId)) {
                return user;
            }
        }
        return null;
    }

    public static String format(BigDecimal amount) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        df.setDecimalFormatSymbols(symbols);
        return df.format(amount);
    }
}
