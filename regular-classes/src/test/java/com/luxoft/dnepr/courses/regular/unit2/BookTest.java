package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();

        assertEquals(true,book.equals(cloned));
        cloned.setPrice(15);
        assertEquals(true,book.equals(cloned));
        cloned = null;
        assertEquals(false,book.equals(cloned));


    }

    @Test
    public void testEquals() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 2030, new GregorianCalendar(2006, 0, 1).getTime());
        Book book3 = productFactory.createBook("code", "Thinking in Java 2", 200, new GregorianCalendar(2006, 0, 1).getTime());
        assertEquals(true,book1.equals(book2));
        assertEquals(false,book1.equals(book3));

    }
}
