package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import org.junit.Assert;
import org.junit.Test;

public class BankJavaVersionTest {

    private static final String CURRENT_JAVA_VERSION = System.getProperty("java.version");

    @Test
    public void testExpectedJavaVersionFailure() {
        String expectedJavaVersion = "something_really_wrong";
        try {
            new Bank(expectedJavaVersion);
            Assert.fail("IllegalJavaVersionError must be thrown on something_really_wrong java version");
        } catch (IllegalJavaVersionError error) {
            Assert.assertEquals(expectedJavaVersion, error.getExpectedJavaVersion());
            Assert.assertEquals(CURRENT_JAVA_VERSION, error.getActualJavaVersion());
        }
    }

    @Test
    public void testExpectedJavaVersionOK() {
        new Bank(CURRENT_JAVA_VERSION);
    }
}
