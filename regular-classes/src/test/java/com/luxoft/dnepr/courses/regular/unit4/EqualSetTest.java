package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class EqualSetTest {

    EqualSet<Integer> collection = new EqualSet<>();
    ArrayList<Integer> arrayList = new ArrayList<>();
    ArrayList<Integer> emptyList = new ArrayList<>();

    Integer pew1 = 2;
    Integer pew2 = 3;
    Integer pew3 = 4;
    Integer pew4 = null;
    Integer pew5 = null;
    Integer pew6 = 4;
    Integer pew7 = 111;
    Integer pew8 = 9999999;
    Integer pew9 = 0;


    @Before
    public void init() {

        collection.add(pew1);
        collection.add(pew2);
        collection.add(pew3);
        collection.add(pew4);
        collection.add(pew5);
        collection.add(pew6);
        collection.add(pew7);

        arrayList.add(pew1);
        arrayList.add(pew1);
        arrayList.add(pew3);
        arrayList.add(pew5);
        arrayList.add(pew5);
        arrayList.add(pew8);
        arrayList.add(pew9);
    }

    @Test
    public void testSize() throws Exception {
        assertEquals(5, collection.size());
        collection.remove(pew5);
        assertEquals(4, collection.size());
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertEquals(false, collection.isEmpty());
    }

    @Test
    public void testContains() throws Exception {
        assertEquals(true, collection.contains(pew4));
        assertEquals(false, collection.contains(pew9));
    }

    @Test
    public void testAddAll() throws Exception {
        collection.addAll(arrayList);
        assertEquals(7, collection.size());
    }

    @Test
    public void testRemoveAll() throws Exception {
        collection.removeAll(arrayList);
        assertEquals(2, collection.size());
    }

    @Test
    public void testClear() throws Exception {
        collection.clear();
        assertEquals(0, collection.size());
    }

    @Test
    public void retainAllTest() throws Exception {
        collection.retainAll(arrayList);
        assertEquals(3, collection.size());
        collection.retainAll(emptyList);
        assertEquals(0, collection.size());
    }
}
